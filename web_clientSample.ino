#include <SoftwareSerial.h>
SoftwareSerial GSM(7, 8); // RX, TX

String http_header = "POST /count_update HTTP/1.0";
String http_host = "Host: 52.43.105.226";
String http_content_type = "Content-Type: application/json";
String http_content_length = "Content-Length: ";
String http_connection = "Connection: Keep-Alive";

int count = 0;

int contentLength = 0;

unsigned long previousMillis = 0;        // will store last time LED was updated

// constants won't change :
const long interval = 10000;           // interval at which it call the updatecount (milliseconds)


const int buttonPin = 2;    // the number of the pushbutton pin
const int ledPin = 13;      // the number of the LED pin

// Variables will change:
int ledState = HIGH;         // the current state of the output pin
int buttonState;             // the current reading from the input pin
int lastButtonState = LOW;   // the previous reading from the input pin

// the following variables are long's because the time, measured in miliseconds,
// will quickly become a bigger number than can be stored in an int.
long lastDebounceTime = 0;  // the last time the output pin was toggled
long debounceDelay = 50;    // the debounce time; increase if the output flickers


void setup()
{
  pinMode(buttonPin, INPUT);
  pinMode(ledPin, OUTPUT);

  // set initial LED state
  digitalWrite(ledPin, ledState);

  // To be put in loop
  String request_data = "{\"count\":" + String(count) + ", \"event\":\"mit\", \"gateID\": 1}";
  String request_data_length = String(request_data.length());


  GSM.begin(9600);
  Serial.begin(9600);
  Serial.println("Serial Start");
  GSM.println("AT+CIPMUX=0");

  GSM.println("AT+CSTT=\"uninor\",\"\",\"\"");
  delay(300);


  GSM.println("AT+CIPSTART=\"TCP\",\"52.43.105.226\", \"80\"");
  delay(400);

  // updateCount();
}

void loop()
{

  int reading = digitalRead(buttonPin);

  if (reading != lastButtonState) {
    // reset the debouncing timer
    lastDebounceTime = millis();
  }

  if ((millis() - lastDebounceTime) > debounceDelay) {

    // if the button state has changed:
    if (reading != buttonState) {
      buttonState = reading;

      // only toggle the LED if the new button state is HIGH
      if (buttonState == HIGH) {
        ledState = !ledState;
        count++;
        Serial.println(count);
      }
    }
  }

  // set the LED:
  digitalWrite(ledPin, ledState);

  // save the reading.  Next time through the loop,
  // it'll be the lastButtonState:
  lastButtonState = reading;

  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= interval) {
    // save the last time you blinked the LED
    previousMillis = currentMillis;

    updateCount();

  }



  if (GSM.available())
    Serial.write(GSM.read());
}

void updateCount() { //int count, String eventCode, int gateID
  // To be put in loop
  String request_data = "{\"count\":" + String(count) + ", \"eventCode\":\"mit\", \"gateID\": 1}";
  String request_data_length = String(request_data.length());
  http_content_length = "Content-Length: " + request_data_length;
  String cip_length = String((http_header + http_host + http_content_type + http_content_length + http_connection + request_data).length() + 14);
  GSM.println("AT+CIPSEND=" + cip_length);
  delay(1500);
  GSM.println(http_header);
  GSM.println(http_host);
  GSM.println(http_content_type);
  GSM.println(http_content_length);
  GSM.println(http_connection);
  GSM.println();
  GSM.println(request_data);
  GSM.println();
  GSM.println();
  GSM.write(0x1A);
  Serial.println("Sending");
}




